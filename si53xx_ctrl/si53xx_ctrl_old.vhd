--------------------------------------------------------------------------------
--  HGW RAL Jan 2020.
--  Programs the Si5324 jitter cleaner on KC705
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library work;
use work.support.all;

entity si53xx_ctrl is
generic (
  I2C_INPUT_CLK_G : INTEGER := 100_000_000; -- Input clock [Hz]
  I2C_BUS_CLK_G   : INTEGER := 400_000;     -- I2C clock [Hz]
  I2C_THROTTLE_G  : INTEGER := 125_000;     -- Sysclk's between I2C pkts, 120_000
  ILA_DEBUG_ON_G  : string := "false"
); 
port (
    -- system clock
    CLK_IN                : in    std_logic;
    RESET_IN              : in    std_logic;
    -- I2C Interface
    W_EN_IN               : in std_logic;
    R_EN_IN               : in std_logic;
    SDA                   : inout std_logic;
    SCL                   : inout std_logic;
    SI5324_I2C_W_DONE_OUT : out   std_logic;
    SI5324_RESETB_OUT     : out   std_logic;
    IIC_MUX_RESETB_OUT    : out   std_logic
);
end si53xx_ctrl;


architecture rtl of si53xx_ctrl is

constant KC705_I2C_MUX_ADDR_C : std_logic_vector(6 downto 0) := "1110100";  --  0x74  
constant KC705_I2C_MUX_SETTING_C : std_logic_vector(7 downto 0) := X"80";   -- channel 7 on KC705 I2C Mux. Connect Si5324 
constant SI5324_ADDR_C : std_logic_vector(6 downto 0) := "1101000";  -- 0x68  

type i2c_fsm_t is (INIT_st, INIT_MUX_st, INIT_SYNC_st,              -- set up of I2C mux on KC705
                   WAIT_SI5324_W_st, SYNC_SI5324_W_st, W_SI5324_st, -- writing Si5324
                   WAIT_SI5324_R_st, SYNC_SI5324_R_st, R_SI5324_st, -- readback Si5324
                   FINISHED_st);
                   
                   
type i2c_mem_t is array (natural range <>) of std_logic_vector(15 downto 0);
type i2c_data_t is array (natural range <>) of std_logic_vector(7 downto 0);

-- Play Memory depth
constant W_PLAY_SIZE      : integer := 12; -- number of registers that will be written
constant R_PLAY_SIZE      : integer := 3; -- number of registers that will be read


signal i2c_w_mem: i2c_mem_t(0 to W_PLAY_SIZE-1);
signal i2c_r_mem: i2c_data_t(0 to R_PLAY_SIZE-1);
signal si5324_reg_rds: i2c_mem_t(0 to R_PLAY_SIZE-1);

-- Configuration register writes for 100G clock of 322.265MHz
--constant i2c_mem : i2c_mem_t(0 to PLAY_SIZE-1) := (X"0016");
--signal i2c_w_mem : i2c_mem_t(0 to PLAY_SIZE-1);

signal i2c_fsm          : i2c_fsm_t;
signal busy_prev        : std_logic;
signal i2c_ena          : std_logic;
signal i2c_addr         : std_logic_vector(6 downto 0);
signal i2c_rw           : std_logic;
signal i2c_data_wr      : std_logic_vector(7 downto 0);
signal i2c_busy         : std_logic;
signal i2c_data_rd      : std_logic_vector(7 downto 0);
signal i2c_ack_error    : std_logic;
signal i2c_rise         : std_logic;
--signal i2c_data         : i2c_data_t(7 downto 0);
signal i2c_start        : std_logic;
signal sda_din          : std_logic;
signal scl_din          : std_logic;
signal sda_t            : std_logic;
signal scl_t            : std_logic;
signal i2c_play         : std_logic;
signal windex           : integer range 0 to W_PLAY_SIZE-1;
--signal i2c_done_n       : std_logic;


signal dbg_si5324_reg80 : std_logic_vector(15 downto 0);
signal dbg_si5324_reg81 : std_logic_vector(15 downto 0);
signal dbg_si5324_reg82 : std_logic_vector(15 downto 0);

  attribute mark_debug  : string;
  attribute mark_debug of i2c_ack_error        : signal is ILA_DEBUG_ON_G;
  attribute mark_debug of dbg_si5324_reg80         : signal is ILA_DEBUG_ON_G;
  attribute mark_debug of dbg_si5324_reg81         : signal is ILA_DEBUG_ON_G;
  attribute mark_debug of dbg_si5324_reg82         : signal is ILA_DEBUG_ON_G;


begin

dbg_si5324_reg80 <= si5324_reg_rds(0); -- necessary as vivado doens't like setting an ila on array
dbg_si5324_reg81 <= si5324_reg_rds(1);
dbg_si5324_reg82 <= si5324_reg_rds(2);

IIC_MUX_RESETB_OUT <= '1';  -- I2C mux's active-low reset always 1

--------------------------------------------------------------------
-- I2C Read/Write operations are played from memory,
-- Memory Content stores address and value in 16-bit format.
-- bits[15:8] => Register address
-- bits[ 7:0] => Register value to be written
--------------------------------------------------------------------

--i2c_mem(0) <= X"0016";  -- This enables  bypass mode

-- The values of registers are adjusted to 156.25 MHz in/out , MSB 2 bytes are the reg adrr while the LSB 2 bytes are the reg data
i2c_w_mem <= (
X"0272",  -- write reg 2 with 72   bandwidth select BWSEL
x"1940", -- Write Reg 25 to set N1_HS = 6
x"1F00", -- Write Regs 31,32,33 to set NC1_LS = 6
x"2000",
x"2105",
x"28A0",  --Write Regs 40,41,42 to set N2_HS = 9, N2_LS = 316
x"2901",
x"2A3B",
x"2B00",  --Write Regs 43,44,45 to set N31 = 79
x"2C00",
x"2D4E",
x"8840"   -- Write Reg 136 to set ICAL = 1  , starting calibration process
); 

i2c_r_mem <= (
x"80",  -- reg 128 - Si5324 Clock 1,2 Active 
x"81",  -- reg 129 - Si5324 LOS1,2,X 
x"82"
);

--------------------------------------------------------------------
-- 3-state I2C I/O Buffers
--------------------------------------------------------------------
iobuf_scl : iobuf
port map (
    I  => '0',
    O  => scl_din,
    IO => scl,
    T  => scl_t
);

iobuf_sda : iobuf
port map (
    I  => '0',
    O  => sda_din,
    IO => sda,
    T  => sda_t
);

-- I2C Master Device 
i2c_master_inst : entity work.i2c_master
generic map (
    input_clk   => I2C_INPUT_CLK_G,
    bus_clk     => I2C_BUS_CLK_G
)
port map (
    clk         => CLK_IN,
    reset       => RESET_IN,
    ena         => i2c_ena,
    addr        => i2c_addr,
    rw          => i2c_rw,
    data_wr     => i2c_data_wr,
    busy        => i2c_busy,
    data_rd     => i2c_data_rd,
    ack_error   => i2c_ack_error,
    sda         => sda_din,
    scl         => scl_din,
    sda_t       => sda_t,
    scl_t       => scl_t
);

-- Throttle down I2C packets just to be on the safe side
start_presc : entity work.prescaler
generic map (
    I2C_THROTTLE_G  => I2C_THROTTLE_G     -- Sysclk's between I2C pkts, 120_000;     -- Sysclk's between I2C pkts, 120_000
    )
port map (
    clk_i       => CLK_IN,
    reset_i     => RESET_IN,
    pulse_o     => i2c_start
);

i2c_rise <= i2c_busy and not busy_prev;

process(CLK_IN)
    variable busy_cnt         : natural range 0 to 15;
begin
    if rising_edge(CLK_IN) then
        if (RESET_IN = '1') then
            i2c_fsm <= INIT_st;
            windex <= 0;
            busy_cnt := 0;
            busy_prev <= '1';
            i2c_rw <= '1';
            i2c_ena <= '0';
            i2c_addr <= (others => '0');
            i2c_data_wr <= X"00";
            SI5324_I2C_W_DONE_OUT <= '0';
        else
            busy_prev <= i2c_busy;

            case (i2c_fsm) is
                -- Wait I2C master to start
                
       --  ***************   initialiase I2C Mux on KC705 ***********************         
                when INIT_st =>
                    if (i2c_busy = '0' and i2c_start = '1') then
                        i2c_fsm <= INIT_MUX_st;      -- Injitialise the U49 I2C Mux on the KC705 boatd
                        SI5324_RESETB_OUT <='0';     
                    end if;

                when INIT_MUX_st =>
                    if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= KC705_I2C_MUX_ADDR_C ;
                            i2c_rw <= '0';          -- Write cmd
                            i2c_data_wr <= KC705_I2C_MUX_SETTING_C ;
                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= INIT_SYNC_st;
                            end if;
                        when others => NULL;
                    end case;

                when INIT_SYNC_st =>
                    if (i2c_start = '1') then
                        i2c_fsm <= WAIT_SI5324_W_st;
                    end if;
                    SI5324_RESETB_OUT <='1';
                    
--  ***********************  Write Si5324 registers **********
               
               -- wait for permission to proceed with write
                when WAIT_SI5324_W_st => 
                  if W_EN_IN = '1' then
                    i2c_fsm <= SYNC_SI5324_W_st; 
                  end if;   
               
                when SYNC_SI5324_W_st =>
                    if (i2c_start = '1') then
                        i2c_fsm <= W_SI5324_st;
                    end if;
               
                when W_SI5324_st =>
                    if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= SI5324_ADDR_C;  
                            i2c_rw <= '0';                                 -- write
                            i2c_data_wr <= i2c_w_mem(windex)(15 downto 8); -- send register address 
  
                        when 1 =>
                            i2c_data_wr <= i2c_w_mem(windex)(7 downto 0);  -- send data to be written

                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                if (windex = W_PLAY_SIZE - 1) then         -- continue until all valswritten
                                    SI5324_I2C_W_DONE_OUT <= '1';
                                    i2c_fsm <= WAIT_SI5324_R_st;
                                else
                                    windex <= windex + 1;    
                                    i2c_fsm <= SYNC_SI5324_W_st;  
                                end if;
                            end if;

                        when others => NULL;
                    end case;
                    
       -- *********************  Read Back Si5324 registers  ***********
    
                when WAIT_SI5324_R_st => -- wait for permission to read back
                  windex <= 0;
                  if R_EN_IN = '1' then
                    i2c_fsm <= SYNC_SI5324_R_st; 
                  end if;
                  
                when  SYNC_SI5324_R_st  =>
                  if (i2c_start = '1') then
                    i2c_fsm <=  R_SI5324_st;
                  end if;     

                when  R_SI5324_st =>
                    if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
                    
                     case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= SI5324_ADDR_C;  
                            i2c_data_wr <= i2c_r_mem(windex); -- target register address
                            i2c_rw <= '0';                    -- send this address (write = 0 )
                            
                        when 1 =>  
                            i2c_rw <= '1';                    -- send read command
                              
                        when 2 =>
                            if i2c_busy = '0' then           -- Read data and store, together with address, in si5324_reg_rds
                                si5324_reg_rds(windex)  <=  i2c_r_mem(windex) & i2c_data_rd;
                            end if;

                        -- Continue until all values are read
                        when 3 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                if (windex = R_PLAY_SIZE - 1) then
                                    i2c_fsm <= FINISHED_st;
                                else
                                    windex <= windex + 1;    
                                    i2c_fsm <= SYNC_SI5324_R_st;
                                end if;
                            end if;
                        when others => NULL;
                    end case;

                when FINISHED_st =>
                    if R_EN_IN = '1' then  -- continue to read Si5324 whilst R_EN = '1'
                      i2c_fsm <= SYNC_SI5324_R_st; 
                    end if;

                when others => NULL;

            end case;
        end if;
    end if;
end process;


end rtl;
